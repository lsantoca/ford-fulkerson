class Graph:

    def __init__(self, size, name=None):
        self.size = size
        self.name = name
        self.inc_incoming = []
        self.inc_outgoing = []
        for i in range(0, self.size):
            self.inc_incoming.append([])
            self.inc_outgoing.append([])

    def getVertexes(self):
        return range(0, self.size)

    def arcs_out(self, vertex):
        return self.inc_outgoing[vertex]

    def arcs_in(self, vertex):
        return self.inc_incoming[vertex]

    def add_arc(self, arc):
        if isinstance(arc, str) or isinstance(arc, tuple):
            src = int(arc[0])
            dst = int(arc[1])
            arc = Arc(None, src, dst)

        self.inc_incoming[arc.dst].append(arc)
        self.inc_outgoing[arc.dst].append(arc)

    def getArcs(self, vertex):
        return self.inc_outgoing[vertex] + self.inc_incoming[vertex]

    def add_arcs(self, arcs):
        for arc in arcs:
            self.add_arc(arc)

    def getAllArcs(self):
        return [arc for vertex in self.getVertexes() for arc in self.inc_outgoing[vertex]]
