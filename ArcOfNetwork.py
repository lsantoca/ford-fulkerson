from Arc import Arc


class ArcOfNetwork(Arc):

    def __init__(self, label, src, dst, capacity):
        super().__init__(label, src, dst)
        self.flow = 0
        self.capacity = capacity

    def getFlow(self):
        return self.flow

    def getCapacity(self):
        return self.capacity

    def setFlow(self, flow):
        self.flow = flow

    def isForward(self, source):
        return source == self.src

    def isBackward(self, source):
        return source == self.dst

    def getOther(self, source):
        if self.isForward(source):
            return self.dst
        else:
            return self.src

    def resCapacity(self, source):
        if self.isForward(source):
            return self.capacity - self.flow
        else:
            return self.flow

    def updateFlow(self, source, val):
        if self.isForward(source):
            self.setFlow(self.flow + val)
        else:
            self.setFlow(self.flow - val)

    def isTraversable(self, source):
        if self.isForward(source):
            return self.getFlow() < self.getCapacity()
        else:
            return self.getFlow() > 0
