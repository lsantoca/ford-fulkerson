import Graph
import ArcOfNetwork


class Network(Graph.Graph):

    def __init__(self, size, source=0, target=None, name=None, bfs=True):
        super().__init__(size, name)
        self.source = source
        if target is None:
            self.target = size - 1
        else:
            self.target = target
        self.maxCapacity = 0
        self.bfs = bfs

    def exploreFromSource(self):
        visited = set()
        frontier = []
        parent = {}

        def explore(vertex, arc):
            newVertex = arc.getOther(vertex)

            if newVertex not in visited:
                visited.add(newVertex)
                parent[newVertex] = (vertex, arc)
                for arc in self.getArcs(newVertex):
                    if arc.isTraversable(newVertex):
                        frontier.append((newVertex, arc))

        visited.add(self.source)
        parent[self.source] = None
        for arc in self.getArcs(self.source):
            if arc.isTraversable(self.source):
                frontier.append((self.source, arc))

        while frontier:
            if self.bfs:
                (vertex, arc) = frontier.pop(0)
            else:
                (vertex, arc) = frontier.pop(-1)
            explore(vertex, arc)

        return parent

    def FordFulkerson(self):
        for arc in self.getAllArcs():
            arc.setFlow(0)

        while True:
            self.trace() ; self.trace(residual=True)

            parent = self.exploreFromSource()

            if self.target not in parent:
                return
            path = self.getPath(parent); self.trace(path=path)
            val = self.minResidualCapacity(path)
            self.updateFlow(path, val); self.trace(path=path)

    def getPath(self, parent) -> list():
        path = []
        vertex = self.target
        while True:
            pair = parent[vertex]
            if pair is None:
                return path
            path.append(pair)
            vertex = pair[0]
        return path

    def minResidualCapacity(self, path) -> int:
        minResCap = self.maxCapacity
        for (vertex, arc) in path:
            minResCap = min(minResCap, arc.resCapacity(vertex))
        return minResCap

    def updateFlow(self, path, val):
        for pair in path:
            (vertex, arc) = pair
            arc.updateFlow(vertex, val)

    def add_arc(self, triple):
        (src, capacity, dst) = triple
        arc = ArcOfNetwork.ArcOfNetwork(
            None, src, dst, capacity
        )

        self.inc_incoming[arc.dst].append(arc)
        self.inc_outgoing[arc.src].append(arc)
        self.maxCapacity = max(capacity, self.maxCapacity)

    def trace(self, parent=None, path=None, residual=False) -> None:
        pass
