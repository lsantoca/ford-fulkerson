class Arc:

    def __init__(self, label, src, dst):
        self.label=label
        self.src=src
        self.dst=dst

    def getSrc(self):
        return self.src

    def getDst(self):
        return self.dst

    def setLabel(self, label):
        self.label=label
