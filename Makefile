exec:
	python Main.py

zip:
	cd ../ ; zip FordFulkerson.zip FF/* ; mv FordFulkerson.zip FF/

clean:
	rm -r figs/*
