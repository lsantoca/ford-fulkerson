import pip
import sys

try:
    __import__("graphviz")
except ImportError:
    print("You need to install the grahviz package. Use :\n > pip install graphviz")
    sys.exit(1) 

from NetworkDemo import NetworkDemo



def demo1():
    maxCap = 500
    arcs = [(0, maxCap, 2), (0, maxCap, 1), (1, maxCap, 3), (2, maxCap, 3), (1, 1, 2)]
    network = NetworkDemo(
        size=4,
        name="firstDemo",
        install_path=myPath+"FirstDemo",
        bfs=False
    )
    network.set_positions(
        {0: (0, 0),
         1: (2, 1),
         2: (2, -1),
         3: (4, 0)
         })
    network.add_arcs(arcs);
    network.FordFulkerson()


def demo2():
    arcs = [(0, 8, 1), (0, 9, 4),
            (1, 3, 2), (1, 5, 3), (1, 3, 4),
            (2, 2, 5), (2, 7, 6),
            (3, 1, 5), (3, 6, 2),
            (4, 2, 3), (4, 7, 5),
            (5, 10, 6)
            ]
    network = NetworkDemo(
        size=7,
        name="exo2TD3",
        install_path=myPath+"TD3",
        # bfs = False
    )
    network.add_arcs(arcs);
    network.set_positions(
        {0: (0, 0),
         1: (1,1),
         2: (3,1),
         3: (2,0),
         4: (1,-1),
         5: (3,-1),
         6: (4, 0)
         })
    network.FordFulkerson()

def demo3():
    arcs = [(0, 3, 1), (0, 6, 2), (0, 9, 3),
            (1, 11, 4), (1, 4, 5),
            (2, 8, 1), (2, 4, 3), (2, 7, 5),
            (3, 3, 5), (3, 5, 6),
            (4, 2, 5), (4, 10, 7),
            (5, 5, 7),
            (6, 3, 5), (6, 2, 7),
            ]
    network = NetworkDemo(
        size=8,
        name="Polycopie",
        install_path=myPath+"Polycopie",
        # bfs = False
    )
    network.add_arcs(arcs);
    network.set_positions(
        {0: (0, 0),
         1: (2,1),
         2: (2,0),
         3: (2,-1),
         4: (4,1),
         5: (4,0),
         6: (4,-1),
         7: (6, 0),
         })
    network.FordFulkerson()

def demo4():
    arcs = [(0, 1, 1), (0, 1, 2), (0, 1, 3), (0, 1, 4),
            (1,1,5), (1,1,6),
            (2,1,6), (2,1,7),
            (3,1,7), (3,1,8),
            (4,1,8),
            (5,1,9), (6,1,9), (7,1,9), (8,1,9),
            ]
    network = NetworkDemo(
        size=10,
        name="Bipartite",
        install_path=myPath+"Bipartite",
        # bfs = False
    )
    network.add_arcs(arcs);
    network.set_positions(
        {0: (0, 1.5),
         1: (2,3),
         2: (2,2),
         3: (2,1),
         4: (2,0),
         5: (4,3),
         6: (4,2),
         7: (4,1),
         8: (4,0),
         9: (6,1.5)
         })
    network.FordFulkerson()

if __name__ == "__main__":

    myPath= "./figs/"
    if len(sys.argv) > 1 and sys.argv[1] == "ls":
        myPath="/Users/lsantoca/Workspace/AlgoGraphes/latex/ch04/figs/"

    demo1()
    demo2()
    demo3()
    demo4()
