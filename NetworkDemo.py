import graphviz
import Network

class NetworkDemo(Network.Network):

    def __init__(self, size, source=0,
                 target=None, name=None, install_path=None, bfs=True,
                 engine="neato"):
        super().__init__(size, source, target=target, name=name, bfs=bfs)
        self.install_path = install_path
        self.no_iteration = 0
        self.position = {}
        self.engine=engine

    def trace(self, parent=None, path=None, residual=False):
        print("Producing slide ", self.no_iteration)
        if residual:
            self.draw_residual()
        else:
            self.draw_network(path)
        self.no_iteration = self.no_iteration + 1

    def draw_residual(self):
        dot = self.dot_initialize()
        for arc in self.getAllArcs():
            src = arc.src
            label = "{}".format(arc.resCapacity(src))
            if arc.isTraversable(src):
                dot.edge("{}".format(arc.src), "{}".format(arc.dst), color="blue", label=label)
            else:
                dot.edge("{}".format(arc.src), "{}".format(arc.dst), penwidth="0", arrowhead="none")

        for arc in self.getAllArcs():
            src = arc.src
            dst = arc.dst
            label = "{}".format(arc.resCapacity(dst))
            if arc.isTraversable(dst):
                dot.edge("{}".format(arc.dst), "{}".format(arc.src), color="red", label=label, constrain="false",
                         constraint="false")
        self.dot_render(dot)

    def draw_network(self, path=None):
        dot = self.dot_initialize()

        for arc in self.getAllArcs():
            src=arc.src
            dst=arc.dst
            color = None
            penwidth = "2"
            vertex = self.arcLookUp(arc, path)
            if vertex is not None:
                penwidth = "5"
                if arc.isForward(vertex):
                    color = "blue"
                else:
                    dst=src
                    src=arc.dst
                    color = "red"
            label = "{}|{}".format(arc.getFlow(), arc.getCapacity())
            dot.edge("{}".format(src), "{}".format(dst), color=color, label=label, penwidth=penwidth)

        self.dot_render(dot)

    def dot_render(self, dot):
        name = self.name
        dir = self.install_path
        if dir is None:
            dir = name
        path = "{dir}/{prefix}_{no:03d}".format(
            dir=dir,
            prefix=name,
            no=self.no_iteration
        )
        dot.render(path, view=False)

    def dot_initialize(self):
        dot = graphviz.Digraph(comment=self.name, engine=self.engine)
        dot.attr('node', style="filled")
        dot.attr('graph', rankdir="LR")
        dot.attr('edge', penwidth="2")

        for vertex in self.getVertexes():
            name = "{}".format(vertex)
            fill_color = "white"
            text_color = "black"
            pos = None
            if vertex in self.position:
                pos = "{},{}!".format(self.position[vertex][0], self.position[vertex][1])

            if vertex == self.source:
                label = "s"
            elif vertex == self.target:
                label = "t"
            else:
                label = "{}".format(name)

            dot.node(name, fillcolor=fill_color, fontcolor=text_color, label=label, pos=pos)
        return dot

    def arcLookUp(self, a, path):
        if path is None:
            return None
        for (vertex, arc) in path:
            if a == arc:
                return vertex
        return None

    def set_positions(self, positions):
        for (vertex,pos) in positions.items():
            self.position[vertex] = pos


